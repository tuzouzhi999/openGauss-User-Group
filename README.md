## openGauss User Group
openGauss User Group，简称oGUG，是一个让openGauss用户就技术特性、最佳实践、运营进展等方向交流的开放性本地社区。

## oGUG角色介绍
 oGUG由Organizer 、Member、Ambassador三种角色构成:
* Organizer：oGUG区域负责人，负责统筹本区域内oGUG 的发展以及活动规划;
* Member：配合区域oGUG的日常运营，以用户身份产出技术内容、参加活动、推广openGauss。
* Ambassador：通过布道的方式帮助他人了解或使用openGauss，并代表用户优化产品体验，增进其他用户对openGauss的了解。

## 欢迎加入oGUG
![输入图片说明](https://images.gitee.com/uploads/images/2021/0427/155608_e122fd98_9039100.jpeg "qrcode(1).jpg")
* 链接：https://www.wjx.cn/vm/hUtnVcG.aspx

## 深圳用户组
* Organizer 海量数据 李景娥                       lije@vastdata.com.cn
* Member 华为 朱彬                               zhubin6@huawei.com
* Member 海量数据 王铭玥	                 wangmy@vastdata.com.cn
* Ambassador 海量数据 林根	                 lingen@vastdata.com.cn
* Ambassador 华为 田文罡	                 tianwengang@huawei.com
* Ambassador 华为 李思昊	                 sean.lisihao@huawei.com
* Ambassador 华为 熊小军	                 xiongxiaojun2@huawei.com
* Ambassador 视源股份 周俊	                 zhoujun@cvte.com

## 南京用户组
* Organizer   江苏鲲鹏·昇腾生态创新中心 董亮	 gavin.dongliang@huawei.com
* Ambassador  南京唯优信息技术有限公司 吴瑞磊       wuruilei@willbingo.com	
* Ambassador  中软国际有限公司 李臻峰              lzfeng2008@aliyun.com	
* Member      个人开发者 党宏博                    yafeishiorcl@gmail.com
  

## 北京用户组
* Organizer	北京鲲鹏联合创新中心 周志	         zhi.zhou@huawei.com
* Ambassador	云和恩墨 张皖川                   wanchuan.zhang@enmotech.com
* Ambassador	云和恩墨 姜殿斌                   dianbin.jiang@enmotech.com
* Ambassador	宝兰德	詹年科	                 nianke.zhan@bessystem.com
* Ambassador	SphereEx 张亮                    zhangliang@sphere-ex.com
* Ambassador	深信服	章明星	                 zhangmingxing@sangfor.com.cn
* Member	海量数据 余江	                 yujiang@vastdata.com.cn
* Member	云和恩墨 王潇                     xiao.wang@enmotech.com


## 西安用户组
* Organizer	陕西鲲鹏生态创新中心 王涛        545188@qq.com
* Member	云和恩墨 刘珲	               hui.liu@enmotech.com
* Member	云和恩墨 季亚斌                 yabin.ji@enmotech.com
* Member	云和恩墨 姚前                   qian.yao@enmotech.com
* Member	宝兰德 敬少飞                   shaofei.jing@dev.bessystem.com
* Member	Gauss松鼠会 康阳                kangyang@huawei.com
* Member	Gauss松鼠会 齐小丰              813304690@qq.com
* Member	杭州沃趣科技 李春               pickup.li@woqutech.com
* Ambassador	云和恩墨 张翠娉                 cuiping.zhang@enmotech.com
* Ambassador	云和恩墨 郭欢                   huan.guo@enmotech.com
* Ambassador	海量数据	石青	               shiqing_Elsa@163.com
* Ambassador	中软国际	李杨	               liyang463@huawei.com

## 长沙用户组
* Organizer	    湖南省鲲鹏生态创新中心 旷俊	     kuangjun1@huawei.com
* Member            中南大学  奎晓燕	 	     xykui@csu.edu.cn
* Member            湖南大学 	谭光华 	             guanghuatan@hnu.edu.cn
* Member            湖南创星科技股份有限公司 姜赳       77604885@qq.com
* Member            中南勘测设计研究院有限公司 李勇	   liyong357@126.com
* Ambassador        湖南省鲲鹏生态创新中心 孙德成	   sundecheng@huawei.com


## 杭州用户组
* Organizer   浙江鲲鹏创新中心 钟启宏                            zhongqihong@huawei.com
* Member      杭州沃趣科技股份有限公司首席架构师  李春            pickup.li@woqutech.com
* Member      杭州沃趣科技股份有限公司 数据库团队负责人 杨禹航     yuh17@sina.com
* Member      浩鲸智能 高级工程师 任壮壮                         770750140@qq.com
* Member      浙江薄冰网络科技有限公司 蒋健                      jiangjian@zjbobingtech.com
* Member      海量数据 赵健                                     zhaojian@vastdata.com.cn   
* Member      中乘启数 唐成                                     tangcheng@csudata.com
* Member      个人开发者  陈海                                  523228587@qq.com
* Ambassador  华为 冯犇                                        fengben@huawei.com
* Ambassador  华为 阙鸣健	                              quemingjian2@huawei.com

## 兰州用户组
* Organizer	 甘肃鲲鹏生态创新中心COO	李浩钧          	lihaojun2@huawei.com
* Ambassador	兰州理工大学教授 赵宏	                594286500@qq.com
* Member	甘肃环讯信息科技有限公司 郭霄	        guoxiao@gshxkj.com.cn
* Member	甘肃鲲鹏生态创新中心 安定                 anding3@huawei.com
* Member	兰州大学 李龙杰	                        ljli@lzu.edu.cn
* Member	中电万维信息技术有限责任公司 赵益寿	603444498@qq.com
* Member	中电万维信息技术有限责任公司 苏胜军	18919312275@189.cn
* Member	云和恩墨（北京）信息技术有限公司 孙雯杰	13909316725@139.com
* Member	云和恩墨（北京）信息技术有限公司 周瑞斌	ruibin.zhou@enmotech.com
* Member	甘肃紫光智能交通与控制技术有限公司	任小斌	rxbyes@163.com
* Member	北京海量数据技术有限公司  姚婷	        yaoting@vastbase.com.cn
* Member	北京海量数据技术有限公司  杨涛	        Yangtao@vastdata.com.cn

## 重庆用户组
* Organizer  重庆鲲鹏创新中心 游欣易        youxinyi@huawei.com 
* Member 重通服数据库 黄经洲               1320336185@qq.com 
* Member 云和恩墨 师庆栋                   qingdong.shi@enmotech.com 
* Member 海量数据 杨红星                   xingxing2066@126.com 
* Member 海量数据 彭向平                    ppsoula@163.com 
* Member 重庆三峡银行股份有限公司 李辉       13638388182@139.com 
* Member 重庆长安民生物流股份有限公司 邓辉   deng.h@139.com 
* Ambassador 云和恩墨 胡毅                 446464087@qq.com 
* Ambassador 云和恩墨 罗炳森               692162374@qq.com 
* Ambassador 云和恩墨 李元鹏               yuanpeng.li@enmotech.com 
* Ambassador 重庆思庄科技有限公司 郑全      zhengquan@cqsztech.com 
* Ambassador 海量数据 陈浩                 chen31577@126.com 
* Ambassador 重庆石油天然气交易中心 刘辉    liuh@chinacqpgx.com 
* Ambassador 重庆大学 盛泳潘               shengyp2011@gmail.com 

## 成都用户组
* Organizer	曾强	四川鲲鹏生态创新中心	zengqiang26@huawei.com
* Ambassador	俞翔	泰克教育	yuxiang@tech-lab.cn
* Ambassador	黄元霞	虚谷伟业	huangyuanxia@xugudb.com
* Ambassador	李真旭	云和恩墨	zhenxu.li@enmotech.com
* Member	佘兴彬	云和恩墨	xingbin.she@enmotech.com
* Member	陈少云	云和恩墨	shaoyun.chen@enmotech.com
* Member	杨明翰	云和恩墨	minghan.yang@enmotech.com
* Member	王涛	云和恩墨	Tao.wang@enmotech.com
* Member	孙久江	海量数据	sunjj@vastdata.com.cn
* Member	舒正英	海量数据	598546998@qq.com
* Member	熊小红	海量数据	xiongxh@vastdata.com.cn
* Member	甘丽	本原数据	li.gan@enmotech.com
* Member	陈泽	本原数据	ze.chen@enmotech.com
* Member	窦尧	兴业数字金融	3397860844@qq.com
* Member	胡娟	兴业数字金融	836583895@qq.com
* Member	刘牌	东方通 	2319492349@qq.com
* Member	熊灿灿	平安科技	xiongcc_1994@126.com
* Member	张钦	商鼎科技	10980903@qq.com
* Member	张国良	中银金科	44288252@qq.com
* Member	江龙滔	北京思斐软件	raigor.jiang@gmail.com
* Member	陈智明	成都海迪鑫华	2415252314@qq.com


## 上海用户组
* Organizer	上海鲲鹏生态创新中心	张中阳	zhangzhongyang3@huawei.com
* Ambassador	个人开发者	宋少华	13919682238@139.com
* Ambassador	民生银行	刘长浩	liuchanghao@cmbc.com.cn
* Ambassador	润和软件信息技术有限公司	刘天顺	Ls9527@126.com
* Ambassador	云和恩墨 	李华	hua.li@enmotech.com
* Member	浪潮软件	李欧	leo79@yeah.net
* Member	海量数据 	明珠禧梅	Mingzxm@vastdata.com.cn
* Member	海量数据 	王钰	wangyu1@vastdata.com.cn
* Member	鹰图软件技术（青岛）有限公司 	周游	you.zhou@hexagon.com
* Member	上交所技术有限责任公司 	姚昕	xyao@sse.com.cn
* Member	信元公司	田介	job-online@163.com
* Member	云和恩墨	胡自贵	zigui.hu@enmotech.com
* Member	海量数据 	黄晶	huangj@vastdata.com.cn
* Member	云和恩墨 	谢金融	jinrong.xie@enmotech.com
